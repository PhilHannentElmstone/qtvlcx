#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLabel>

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsProxyWidget>
#include <QGraphicsRectItem>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDockWidget>
#include <QTimer>
#include <QOpenGLWidget>

#include <QFileDialog>

#include "qtvlcxitem.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	QGraphicsScene * scene = new QGraphicsScene();
	scene->setSceneRect(0, 0, 1920, 1080);

	QGraphicsView * view = new QGraphicsView(scene);
	view->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

	////////////////////////////////////////////////////////////////////////////

	//QOpenGLWidget * glWidget = new QOpenGLWidget();
	//
	//QSurfaceFormat format;
	//format.setSamples(4);
	//glWidget->setFormat(format);
	//
	//view->setViewport(glWidget);
	//
	//view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

	QTransform t;
	t.scale(0.5, 0.5);
	view->setTransform(t);

	////////////////////////////////////////////////////////////////////////////

	this->setCentralWidget(view);

    QList<QTVLCX::Item *> vlcItems;

    QTVLCX::Item * vlcItem = new QTVLCX::Item();
	vlcItems.append(vlcItem);
	scene->addItem(vlcItem);

	vlcItem->setSource("C:\\dev\\qgraphicsvlc\\SampleVideo_1280x720_5mb.mp4");

	////////////////////////////////////////////////////////////////////////////

    QTVLCX::Item * vlcItem2 = 0;
    vlcItem2 = new QTVLCX::Item();
	vlcItems.append(vlcItem2);
	scene->addItem(vlcItem2);
	//
	vlcItem2->moveBy(100, 100);
	//vlcItem2->setRotation(20);
	//
	vlcItem2->setSource("C:\\dev\\qgraphicsvlc\\MyVideo.mp4");

	////////////////////////////////////////////////////////////////////////////

	QWidget * controls = new QWidget();

	QPushButton * b = new QPushButton("Start");


	connect(b, &QPushButton::clicked, [vlcItems]()
	{
        for (QTVLCX::Item * v : vlcItems)
		{
			qDebug() << "v->state() == " << v->state();
			if (v->isPlaying())
			{
				v->pause();
			}
            else if (v->state() == QTVLCX::Paused)
			{
				v->resume();
			}
			else
			{
				v->start();
			}
		}
	});

    connect(vlcItem, &QTVLCX::Item::playing, [b]()
	{
		b->setText("Stop");
	});

    connect(vlcItem, &QTVLCX::Item::paused, [b]()
	{
		b->setText("Start");
	});

    connect(vlcItem, &QTVLCX::Item::stopped, [b]()
	{
		b->setText("Start");
	});

	QHBoxLayout * controlsLayout = new QHBoxLayout();
	controlsLayout->addWidget(b);

	controls->setLayout(controlsLayout);

	QDockWidget * dock = new QDockWidget();
	dock->setWidget(controls);
	dock->setFeatures(QDockWidget::NoDockWidgetFeatures);

	this->addDockWidget(Qt::BottomDockWidgetArea, dock);

	QTimer::singleShot(100, [vlcItem, view]()
	{
		view->centerOn(vlcItem->boundingRect().center());
	});
}

MainWindow::~MainWindow()
{
	delete ui;
}
