#-------------------------------------------------
#
# Project created by QtCreator 2016-03-12T10:54:05
#
#-------------------------------------------------

TARGET = qtvlcx

TEMPLATE = lib

DEFINES += QTVLCX_LIBRARY

QT += widgets gui opengl network

message(" ")
message("=========================================================")

CONFIG(release, debug|release) {
    message("QTVLCX - release")
}
else {
    message("QTVLCX - debug")
}

exists($${CONFIG_ROOT}/qt-build-framework.pri) {
    BUILD_SUB_PROJECT = qtvlcx
    include($${CONFIG_ROOT}/qt-build-framework.pri)
}
else {
    error("requires qt-build-framework")
}

CONFIG += c++11

SOURCES += \
    src/qtvlcxitem.cpp \
    src/qtvlcxplayer.cpp \
    src/qtvlcxplayerthread.cpp \
    src/qtvlcxconversionthread.cpp \
    src/qtvlcxdiscoveryagent.cpp

HEADERS +=\
    include/qtvlcxitem.h \
    src/qtvlcxitem_p.h \
    include/qtvlcxglobal.h \
    include/qtvlcxplayer.h \
    src/qtvlcxplayerthread.h \
    src/qtvlcxconversionthread.h \
    include/qtvlcxdiscoveryagent.h \
    src/qtvlcxdiscoveryagent_p.h

INCLUDEPATH += include

#VLC_ROOT_PATH = C:/Buildbot/vlc-2.2.1-win32

macx|linux-g++ {
    CONFIG += unversioned_libname plugin
}

macx {
    VLC_ROOT_PATH = $$absolute_path($${PWD}/vlc/macx)

    INCLUDEPATH += $${VLC_ROOT_PATH}/include

    QMAKE_SONAME_PREFIX = @rpath
    QMAKE_POST_LINK += install_name_tool -id @rpath/libqtvlcx.dylib $${DESTDIR}/libqtvlcx.dylib $$escape_expand(\n\t)

    # makes the vlc lib includes so that we expect them to be in the same relative folder (flat deployment)
    QMAKE_POST_LINK += install_name_tool -change @loader_path/lib/libvlc.5.dylib @rpath/libvlc.5.dylib $${DESTDIR}/libqtvlcx.dylib $$escape_expand(\n\t)
    QMAKE_POST_LINK += install_name_tool -change @loader_path/lib/libvlccore.8.dylib @rpath/libvlccore.8.dylib $${DESTDIR}/libqtvlcx.dylib $$escape_expand(\n\t)
}
else {

    win32 {
        contains(QMAKE_TARGET.arch, x86_64) {
            VLC_ROOT_PATH = $$absolute_path(vlc/vlc-2.2.4-win64)
        }
        else {
            VLC_ROOT_PATH = $$absolute_path(vlc/vlc-2.2.4-win32)
        }
    }

    linux {
        VLC_ROOT_PATH = $$absolute_path(vlc/linux)
    }

    VLC_SDK = sdk
    INCLUDEPATH += $${VLC_ROOT_PATH}/$${VLC_SDK}/include
    INCLUDEPATH += $${VLC_ROOT_PATH}/$${VLC_SDK}/include/vlc/plugins
}

message(VLC ROOT $${VLC_ROOT_PATH})
message("VLC include paths: " $${INCLUDEPATH})

android {
    contains(CURRENT_BUILD_OS, Windows_NT){
        DEFINES += __MINGW32__
    } else {
        INCLUDEPATH += $${BUILD_LIBRARIES_ROOT}/macx/include
    }

    LIBS += $$absolute_path(vlc/android/libvlc.so)
    LIBS += $$absolute_path(vlc/android/libvlcjni.so)
}
else {
    macx {
        LIBS += $${VLC_ROOT_PATH}/lib/libvlc.5.dylib
        LIBS += $${VLC_ROOT_PATH}/lib/libvlccore.8.dylib
        #LIBS += -llibvlc -llibvlccore
    }
    else {
        LIBS += -L$${VLC_ROOT_PATH}

        *-msvc* {
            #need to include path to libs in path
            LIBS += -L$${VLC_ROOT_PATH}/$${VLC_SDK}/lib
        }

        unix {
            LIBS += $${VLC_ROOT_PATH}/libvlc.so $${VLC_ROOT_PATH}/libvlccore.so.8
            QMAKE_RPATHDIR += $$absolute_path(vlc/linux)
        }
        else {
            LIBS += -llibvlc -llibvlccore
        }
    }
}

CONFIG(debug, debug|release):*-msvc* {
    QMAKE_CXXFLAGS += /Od
}
