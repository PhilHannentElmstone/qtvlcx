#ifndef _QTVLCX_PLAYER_H
#define _QTVLCX_PLAYER_H

#include <QObject>
#include <QMutex>
#include <QSize>
#include <QPixmap>

#include "qtvlcxglobal.h"

//struct libvlc_instance_t;
//struct libvlc_media_player_t;
//struct libvlc_event_t;
//struct libvlc_media_t;
//struct libvlc_log_t;

namespace QTVLCX
{
    class PlayerThread;
    class ConversionThread;

    class QTVLCX_SHARED_EXPORT Player : public QObject
    {

            Q_OBJECT

        public:

            Player(QObject * parent = 0);

            void                closeMedia();

            ////////////////////////////////////////////////////////////////////////

            QString             source() const;
            void                setSource(const QString & source);

            bool                isPlaying() const;

            QPixmap             currentFrame();

            int                 width() const;
            int                 height() const;

            qint64              duration() const;
            qint64              position() const;

            void                debug();

            /// close down / start up any expensive required resources
            void                loadMedia();
            void                unloadMedia();

            int                 volume() const;

        signals:

            void                stopped();
            void                playing();
            void                paused();

            void                frameChanged();

            void                positionChanged(qint64 position);
            void                durationChanged(qint64 duration);

            void                sizeChanged(QSize size);

            void                error(const QString & str = QString());

        public slots:

            void                start();
            void                stop();
            void                pause();
            void                resume();

            void                setPosition(qint64 position);

            void                setLooping(bool loop);

            void                setVolume(int volume);

        private slots:

            void                updatePixmap(const QPixmap pix);

        private:

            void                paintDebugNotifier();

            static QString      processInputString(const QString & str);

            QString             m_source;

            QPixmap             m_pixmap;

            PlayerThread &      m_playerThread;
            ConversionThread &  m_convertThread;
    };
}

#endif // _QTVLCX_PLAYER_H
