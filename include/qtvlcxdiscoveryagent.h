#ifndef QTVLCXDISCOVERYAGENT_H
#define QTVLCXDISCOVERYAGENT_H

#include "qtvlcxglobal.h"

#include <QObject>

namespace QTVLCX
{
    class DiscoveryAgentPrivate;

    class QTVLCX_SHARED_EXPORT DiscoveryAgent : public QObject
    {
            Q_OBJECT

        public:

            explicit DiscoveryAgent(QObject * parent = 0);
            ~DiscoveryAgent();

        signals:

            void        sourceFound(const QString & url, const QString & description);

        public slots:

            void        startDiscovery();
            void        stopDiscovery();

        private:

            DiscoveryAgentPrivate * d_ptr;

            Q_DISABLE_COPY(DiscoveryAgent)
            Q_DECLARE_PRIVATE(DiscoveryAgent)
    };
}

#endif // QTVLCXDISCOVERYAGENT_H
