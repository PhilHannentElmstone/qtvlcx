#ifndef _QTVLCX_ITEM_H
#define _QTVLCX_ITEM_H

#include "qtvlcxglobal.h"

#include <QGraphicsItem>
#include <QSize>

namespace QTVLCX
{
    class ItemPrivate;
    class Player;

    class QTVLCX_SHARED_EXPORT Item : public QGraphicsObject
    {
            Q_OBJECT

            Q_PROPERTY(QString source READ source WRITE setSource)

        public:

            explicit Item(QGraphicsItem * parent = 0);
            ~Item();

            QString         source() const;
            void            setSource(const QString & source);

            bool            isPlaying() const;

            virtual QRectF  boundingRect() const;
            QPainterPath    shape() const;

            QString         getError() const;

            virtual void    paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);

            int             nativeWidth() const;
            int             nativeHeight() const;
            QSize           nativeResolution() const;

            qint64          duration() const;
            qint64          position() const;

            /// close down / start up any expensive required resources
            void            loadMedia();
            void            unloadMedia();

            void            debug();

            int             volume() const;

            QTVLCX::Player * player() const;
            void            redirectPlayerVideo(bool redirect);

        signals:

            void            stopped();
            void            playing();
            void            paused();

            void            sizeChanged(QSize size);

            void            durationChanged(qint64 duration);
            void            positionChanged(qint64 position);

            void            error(const QString & str = QString());

            void            frameChanged();

        public slots:

            void            start();
            void            stop();
            void            pause();
            void            resume();

            void            setPosition(qint64 position);

            void            setLooping(bool loop);

            // 0 == mute, 100 == 0dB
            void            setVolume(int volume);

            void            updateView();

        private:

            ItemPrivate * d_ptr;

            Q_DISABLE_COPY(Item)
            Q_DECLARE_PRIVATE(Item)
    };
}

#endif // _QTVLCX_ITEM_H
