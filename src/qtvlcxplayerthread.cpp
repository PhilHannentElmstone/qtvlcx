#include "qtvlcxplayerthread.h"

#include "qtvlcxglobal.h"

#include <QImage>
#include <QFileInfo>
#include <QTime>
#include <QNetworkProxy>

#include <QDebug>

#include <thread>
#include <sstream>
#include <vector>

QTVLCX::PlayerThread::PlayerThread(QObject *parent)
    : QThread(parent),
      m_state(PlayerState::BootStrapping),
      m_width(0),
      m_height(0),
      m_pausePosition(0),
      m_checkPosition(false),
      m_stopping(false),
      m_loop(false),
      m_volume(100),
      m_waitTS(0),
      m_closeOnAbort(true),
      m_parentDeadAbort(false),
      m_configured(false),
      m_mediaPlayer(0),
      m_mediaListPlayer(0),
      m_mediaList(0),
      m_media(0),
      m_vlcInstance(0),
      m_commandMutex(QMutex::NonRecursive),

#ifdef Q_OS_ANDROID
      m_backingImgA(0),
      m_backingImgB(0),
      m_fillPtr(0)
#else
      m_pixelPtr(0),
      m_pixelPtrAlt(0)
#endif
{
    qRegisterMetaType<const libvlc_event_t *>("const libvlc_event_t *");

    //connect(this, &QThread::finished, this, []()
    //{
    //    qDebug() << "QTVLCX::PlayerThread - thread has stopped";
    //});
    connect(this, &PlayerThread::error, this, [](const QString & str)
    {
        qWarning() << "QTVLCX::PlayerThread - error: " << str;
    });
}

QTVLCX::PlayerThread::~PlayerThread()
{
    m_parentDeadAbort = true;
    this->blockWaitAbort();
    qInfo() << "QTVLCX::~PlayerThread - thread finished";
}

void QTVLCX::PlayerThread::blockWaitAbort()
{
    m_commandMutex.lock();
    m_q.clear();
    m_q.append(PlayerEvent(PlayerCommand::HardAbort));
    m_commandMutex.unlock();

    m_condition.wakeOne();

    this->wait();
}

void QTVLCX::PlayerThread::softAbort(bool block)
{
    m_commandMutex.lock();
    m_q.clear();
    m_q.append(PlayerEvent(PlayerCommand::SoftAbort));
    m_commandMutex.unlock();

    m_condition.wakeOne();

    if (block)
    {
        this->wait();
    }
}

void QTVLCX::PlayerThread::conditionalRestart()
{
    if (!this->isRunning())
    {
        if (m_configured)
        {
            m_state = PlayerState::Stopped;
            m_q.clear();
            m_waitTS = 0;
            this->start();
        }
        else
        {
            this->configure(m_source);
        }
    }
}

void QTVLCX::PlayerThread::debug()
{
    qDebug() << "QTVLCX::PlayerThread Source: " << m_source;
    qDebug() << "QTVLCX::PlayerThread State: " << m_state;

    if (m_mediaListPlayer != 0)
    {
        qDebug() << "QTVLCX::PlayerThread Player State: " << libvlc_media_list_player_get_state(m_mediaListPlayer);
    }

    qDebug() << "QTVLCX::PlayerThread Pause Pos: " << m_pausePosition;
    qDebug() << "QTVLCX::PlayerThread Check Pos: " << m_checkPosition;
    qDebug() << "QTVLCX::PlayerThread Looping: " << m_loop;
    qDebug() << "QTVLCX::PlayerThread Pending Commands: " << m_q.size();

    qDebug() << "QTVLCX::PlayerThread isRunning: " << this->isRunning();
}

QString QTVLCX::PlayerThread::source() const
{
    return m_source;
}

void QTVLCX::PlayerThread::setSource(const QString & source)
{
    m_configured = false;

    if (this->isRunning())
    {
        // the we need to stop and reset
        this->configure(source);
    }
    else
    {
        m_source = source;
    }
}

void QTVLCX::PlayerThread::configure(const QString & source)
{
    if (this->isRunning())
    {
        this->blockWaitAbort();
    }

    // reset as required and start thread again

    m_commandMutex.lock();
    m_q.clear();
    m_commandMutex.unlock();

    m_source = source;
    m_state = PlayerState::BootStrapping;

    m_configured = true;

    this->start();
}

void QTVLCX::PlayerThread::startPlayer()
{
    // only start if not configured. if we are configured we are in soft abort
    if (!this->isRunning() && !m_configured)
    {
        this->configure(m_source);
    }

    this->appendCommand(PlayerCommand::Start);
}

void QTVLCX::PlayerThread::stopPlayer()
{
    this->appendCommand(PlayerCommand::Stop);
}

void QTVLCX::PlayerThread::pausePlayer()
{
    this->appendCommand(PlayerCommand::Pause);
}

void QTVLCX::PlayerThread::resumePlayer()
{
    // only start if not configured. if we are configured we are in soft abort
    if (!this->isRunning() && !m_configured)
    {
        this->configure(m_source);
    }

    this->appendCommand(PlayerCommand::Resume);
}

void QTVLCX::PlayerThread::appendCommand(PlayerCommand cmd, QVariant data)
{
    if (m_state == PlayerState::Aborted || m_state == PlayerState::Aborting)
    {
        return;
    }

    /////////////////////////////////////////////////////////////////////////////

    // this makes m_q a priority queue where abort is always first, checkEnd is prepended,
    // and append otherwise

    m_commandMutex.lock();

    if (cmd == PlayerCommand::HardAbort)
    {
        m_q.prepend(PlayerEvent(cmd, data));
    }
    else if (cmd == PlayerCommand::CheckEnd && !m_q.isEmpty())
    {
        int i = 0;
        while (m_q[i].command == PlayerCommand::HardAbort)
        {
            i++;
        }

        m_q.insert(i, PlayerEvent(cmd, data));
    }
    else
    {
        m_q.append(PlayerEvent(cmd, data));
    }

    m_commandMutex.unlock();

    this->eventCompress();
}

void QTVLCX::PlayerThread::eventCompress()
{
    m_commandMutex.lock();
    // the big one is setPosition because this can cause a delay in the UI
    // during a slider seek and vlc can complain about an input buffer overrun

    // start at the end, find the first setPosition, delete all before it
    bool startDeleting = false;
    for (int i = m_q.size() - 1; i >= 0; i--)
    {
        if (m_q.at(i).command == PlayerCommand::SetPosition)
        {
            if (startDeleting)
            {
                m_q.removeAt(i);
            }

            startDeleting = true;
        }
    }
    m_commandMutex.unlock();
}

qint64 QTVLCX::PlayerThread::duration() const
{
    return m_mediaPlayer == 0 ? 0 : static_cast<qint64>(libvlc_media_player_get_length(m_mediaPlayer));
}

qint64 QTVLCX::PlayerThread::position() const
{
    return m_mediaPlayer == 0 ? 0 : static_cast<qint64>(libvlc_media_player_get_time(m_mediaPlayer));
}

bool QTVLCX::PlayerThread::isPlaying() const
{
    if (m_mediaListPlayer == 0)
    {
        return false;
    }

    return libvlc_media_list_player_get_state(m_mediaListPlayer) == libvlc_Playing;
}

void QTVLCX::PlayerThread::setPosition(qint64 position)
{
    this->appendCommand(PlayerCommand::SetPosition, position);
}

void QTVLCX::PlayerThread::setLooping(bool loop)
{
    // this doesnt need to be a command, its just a lookup at the end reached callback
    m_loop = loop;
}

int QTVLCX::PlayerThread::volume() const
{
    return m_volume;
}

void QTVLCX::PlayerThread::setVolume(int volume)
{
    m_volume = qMin<int>(qMax<int>(volume, 0), 100);
    this->appendCommand(PlayerCommand::SetVolume);
}

void QTVLCX::PlayerThread::ensureHasVLCInstance()
{
    this->closeVLC();

    ////////////////////////////////////////////////////////////////////////////

    std::vector<const char *> v = {"--no-xlib",
                                   "--network-caching=5000",
                                   "--no-video-title-show",
                                   "--disable-screensaver"};

#ifndef Q_OS_MAX
    v.push_back("--avcodec-hw=any");
#endif

//#ifdef QT_DEBUG
//    v.push_back("--verbose=2");
//    v.push_back("-vvv");
//#endif

#ifndef Q_OS_ANDROID

    bool hasVideo = true;

    // default to video if unknown or known video type. inversly, this means only
    // switch to audio mode if it is a known audio type

    static const QList<QString> knownAudioTypes = {"mp3",
                                                   "flac",
                                                   "wav",
                                                   "ogg",
                                                   "wma",
                                                   "m4a",
                                                   "ra"};

    for (int i = 0; i < knownAudioTypes.size() && hasVideo; i++)
    {
        hasVideo = !m_source.endsWith(knownAudioTypes[i]);
    }

    if (!hasVideo)
    {
        v.push_back("--audio-visual=visual");
        v.push_back("--effect-list=spectrometer");
    }

#endif

    m_vlcInstance = libvlc_new((int)v.size(), reinterpret_cast<const char * const *>(v.data()));
}

void QTVLCX::PlayerThread::ensureHasMediaPlayer()
{
    // have to do this first
    this->closeMedia();

    if (m_mediaPlayer != 0)
    {
        libvlc_media_list_player_release(m_mediaListPlayer);
        m_mediaListPlayer = 0;
        libvlc_media_player_release(m_mediaPlayer);
        m_mediaPlayer = 0;
    }

    m_mediaPlayer = libvlc_media_player_new(m_vlcInstance);
    m_mediaListPlayer = libvlc_media_list_player_new(m_vlcInstance);

    libvlc_video_set_format_callbacks(m_mediaPlayer, vlcCBFormat, NULL);
    libvlc_video_set_callbacks(m_mediaPlayer, vlcCBLock, vlcCBUnlock, vlcCBDisplay, this);

    libvlc_event_manager_t * evtManager = libvlc_media_player_event_manager(m_mediaPlayer);

    libvlc_event_attach(evtManager, libvlc_MediaPlayerPlaying, vlcCBEvent, this);
    libvlc_event_attach(evtManager, libvlc_MediaPlayerPaused, vlcCBEvent, this);
    libvlc_event_attach(evtManager, libvlc_MediaPlayerStopped, vlcCBEvent, this);
    libvlc_event_attach(evtManager, libvlc_MediaPlayerEndReached, vlcCBEvent, this);
    libvlc_event_attach(evtManager, libvlc_MediaPlayerEncounteredError, vlcCBEvent, this);
    libvlc_event_attach(evtManager, libvlc_MediaPlayerTimeChanged, vlcCBEvent, this);
    libvlc_event_attach(evtManager, libvlc_MediaPlayerLengthChanged, vlcCBEvent, this);

    libvlc_log_set(m_vlcInstance, vlcLogEvent, this);

    libvlc_media_list_player_set_media_player(m_mediaListPlayer, m_mediaPlayer);

    libvlc_audio_set_volume(m_mediaPlayer, m_volume);
}

bool QTVLCX::PlayerThread::setSourceFromCached()
{
    this->closeMedia();

    ///////////////////////////////////////////////////////////////////////////

    if (m_source.startsWith("dshow://"))
    {
        QString procPath = m_source.mid(8);
        m_media = libvlc_media_new_location(m_vlcInstance, "dshow://");

        QStringList options = procPath.split(":", QString::SkipEmptyParts);

        for (const QString & o : options)
        {
            QString option = ":" + o;
            option = option.trimmed();

            libvlc_media_add_option(m_media, option.toUtf8().constData());
        }
    }
    else
    {
        // need to do this differently depending on if it is a file etc
        QFileInfo mediaInfo(m_source);

        if (mediaInfo.exists())
        {
            m_media = libvlc_media_new_path(m_vlcInstance, m_source.toUtf8().constData());
        }
        else
        {
            QString s = m_source;

            // stupid case sensitivity -> thinks Https goes to port 80 instead of 443
            if (s.startsWith("http", Qt::CaseInsensitive))
            {
                s.replace(0, 4, "http");
            }

            m_media = libvlc_media_new_location(m_vlcInstance, s.toUtf8().constData());
        }
    }

    if (m_media == 0)
    {
        return false;
    }

    // add proxy options
    // https://stackoverflow.com/questions/22661678/run-vlc-with-and-without-proxy-within-one-process

    if (m_source.startsWith("http", Qt::CaseInsensitive))
    {
        QString proxyOptions;
        QNetworkProxy appProxy = QNetworkProxy::applicationProxy();

        if (appProxy.type() != QNetworkProxy::NoProxy &&
            appProxy.type() != QNetworkProxy::FtpCachingProxy &&
            appProxy.type() != QNetworkProxy::DefaultProxy) // if it is default, assume non as we cannot work out its type
        {
            if (appProxy.type() == QNetworkProxy::Socks5Proxy)
            {
                // SOCKS
                proxyOptions = ":socks=" + appProxy.hostName() +
                               ":socks-user=" + appProxy.user() +
                               ":socks-pwd=" + appProxy.password();
            }
            else
            {
                // HTTP
                if (!appProxy.user().isEmpty())
                {
                    proxyOptions = "http-proxy=http://" + appProxy.user() + "@" + appProxy.hostName() + ":" + QString::number(appProxy.port()) + "/";
                }
                else
                {
                    proxyOptions = "http-proxy=http://" + appProxy.hostName() + ":" + QString::number(appProxy.port()) + "/";
                }

                if (!appProxy.password().isEmpty())
                {
                    proxyOptions += ":http-proxy-pwd=" + appProxy.password();
                }
            }

            qInfo() << "adding proxy opions to remote media";

            libvlc_media_add_option(m_media, proxyOptions.toUtf8().constData());
        }
    }

    m_mediaList = libvlc_media_list_new(m_vlcInstance);

    libvlc_media_list_add_media(m_mediaList, m_media);
    libvlc_media_list_player_set_media_list(m_mediaListPlayer, m_mediaList);

    return true;
}

void QTVLCX::PlayerThread::vlcLogEvent(void *data, int level, const libvlc_log_t *ctx, const char *fmt, va_list args)
{
    Q_UNUSED(data);
    Q_UNUSED(ctx);

    if (level == LIBVLC_NOTICE)
    {
        //qDebug() << "QTVLCX::PlayerThread::vlcLogEvent: " << level << " -> " << QString::vasprintf(fmt, args);
    }
    else if (level == LIBVLC_WARNING)
    {
        qWarning() << "QTVLCX::PlayerThread::vlcLogEvent: WARNING: " << level << " -> " << QString::vasprintf(fmt, args);
    }
    else if (level == LIBVLC_ERROR)
    {
        qWarning() << "QTVLCX::PlayerThread::vlcLogEvent: ERROR: " << level << " -> " << QString::vasprintf(fmt, args);
    }
}

void QTVLCX::PlayerThread::closeMedia()
{
    if (m_media != 0)
    {
        if (libvlc_media_list_player_is_playing(m_mediaListPlayer))
        {
            m_stopping = true;
            libvlc_media_list_player_stop(m_mediaListPlayer);
            m_stopping = false;
        }

        libvlc_media_list_release(m_mediaList);
        m_mediaList = 0;

        libvlc_media_release(m_media);
        m_media = 0;
    }
}

void QTVLCX::PlayerThread::closeVLC()
{
    // free the mediaplayer and the vlc instance
    this->closeMedia();

    if (m_mediaPlayer != 0)
    {
        libvlc_media_player_release(m_mediaPlayer);
        m_mediaPlayer = 0;

        libvlc_media_list_player_release(m_mediaListPlayer);
        m_mediaListPlayer = 0;
    }

    if (m_vlcInstance != 0)
    {
        libvlc_release(m_vlcInstance);
        m_vlcInstance = 0;
    }
}

unsigned int QTVLCX::PlayerThread::vlcCBFormat(void ** object, char * chroma, unsigned int * width, unsigned int * height,
        unsigned int * pitches, unsigned int * lines)
{
    QMetaObject::invokeMethod(static_cast<QTVLCX::PlayerThread *>(*object),
                              "setupFormat",
                              //Qt::BlockingQueuedConnection,
                              Qt::DirectConnection,
                              Q_ARG(char*, chroma),
                              Q_ARG(uint*, width),
                              Q_ARG(uint*, height),
                              Q_ARG(uint*, pitches),
                              Q_ARG(uint*, lines));
    return 1;
}

void QTVLCX::PlayerThread::setupFormat(char * chroma, unsigned int * width, unsigned int * height, unsigned int * pitches, unsigned int * lines)
{
    //qDebug() << "QTVLCX : format request:" << chroma << *width << *height;

    // https://wiki.videolan.org/Chroma/

    // QImage does not support YUV format, so we need to convert
    // setting chroma to RV24 should make VLC convert for us
    strcpy(chroma, "RV24");

    pitches[0] = *width * 3;
    lines[0] = *height * 3;
    m_width = *width;
    m_height = *height;

    emit sizeChanged(QSize(m_width, m_height));

#ifdef Q_OS_ANDROID

    if (m_backingImgA != 0)
    {
        delete m_backingImgA;
        delete m_backingImgB;
    }

    m_backingImgA = new QImage(m_width, m_height, QImage::Format_RGB888);
    m_backingImgB = new QImage(m_width, m_height, QImage::Format_RGB888);

#else

    if (m_pixelPtr != 0)
    {
        delete m_pixelBuffA[0];
        delete m_pixelBuffB[0];
    }

    m_pixelBuffA[0] = new char[m_width*m_height*3];
    m_pixelBuffB[0] = new char[m_width*m_height*3];
    m_pixelPtr = m_pixelBuffA[0];
    m_pixelPtrAlt = m_pixelBuffB[0];

#endif

    this->formatUpdated();
}

void QTVLCX::PlayerThread::flipBacking()
{
#ifdef Q_OS_ANDROID
    std::swap(m_backingImgA, m_backingImgB);
#else
    std::swap(m_pixelPtr, m_pixelPtrAlt);
#endif
}

void * QTVLCX::PlayerThread::vlcCBLock(void * object, void ** planes)
{
    // hand the pixel buffer to VLC
    QTVLCX::PlayerThread * instance = static_cast<QTVLCX::PlayerThread *>(object);

#ifdef Q_OS_ANDROID
    // when we swap the ab ptrs, we dont know, so we use fillPtr to keep track of
    // which ptr we used for the display cb
    instance->m_fillPtr = instance->m_backingImgA;
    *planes = (void *)instance->m_backingImgA->constBits();
#else
    *planes = (void *)instance->m_pixelPtr;
#endif

    return 0;
}

void QTVLCX::PlayerThread::vlcCBUnlock(void * object, void * picture, void * const * planes)
{
    Q_UNUSED(object);
    Q_UNUSED(picture);
    Q_UNUSED(planes);

    // this function is not called regularly on all OS's, we can live without locking the
    // resources using double buffering
}

void QTVLCX::PlayerThread::vlcCBDisplay(void * object, void * picture)
{
    Q_UNUSED(picture);

    QTVLCX::PlayerThread * player = static_cast<QTVLCX::PlayerThread *>(object);

    if (player->m_stopping)
    {
        // stop has/will have been called in another thread, so return
        return;
    }

    qint64 position = static_cast<qint64>(libvlc_media_player_get_time(player->m_mediaPlayer));

    if (player->m_checkPosition)
    {
        if (position < player->m_pausePosition)
        {
            //qDebug() << "QTVLCX threw away frame: " << position << " : " << player->m_pausePosition;
            return;
        }

        player->m_checkPosition = false;
    }
    else
    {
        //qDebug() << QTVLC_TID << "QTVLCX - update image";

#ifdef Q_OS_ANDROID
        // calling constBits means we do not do a deep copy, and casting them to
        // non const means that we do not invoke copies further down the line
        emit player->imageReady(QImage((uchar *)player->m_fillPtr->constBits(),
                                       player->m_width,
                                       player->m_height,
                                       QImage::Format_RGB888));
#else
        QImage img((uchar *)player->m_pixelPtr,
                   player->m_width,
                   player->m_height,
                   QImage::Format_RGB888);

        emit player->imageReady(img);
#endif
    }
}

void QTVLCX::PlayerThread::imageConsumed()
{
    this->flipBacking();
}

int QTVLCX::PlayerThread::nativeWidth() const
{
    return m_width;
}

int QTVLCX::PlayerThread::nativeHeight() const
{
    return m_height;
}

void QTVLCX::PlayerThread::vlcCBEvent(const libvlc_event_t * event, void * object)
{
    // see if we can deal with it quickly in this thread

    switch (event->type)
    {
        case libvlc_MediaPlayerTimeChanged:
        {
            //qDebug() << "QTVLCX: position update: " << event->u.media_player_time_changed.new_time;
            emit static_cast<QTVLCX::PlayerThread *>(object)->positionChanged(event->u.media_player_time_changed.new_time);
            return;
        }

        default:
        {
            break;
        }
    }

    libvlc_event_t * tmp = new libvlc_event_t;
    memcpy(tmp, event, sizeof(libvlc_event_t));

#ifdef QT_DEBUG
    static_cast<QTVLCX::PlayerThread *>(object)->m_errString = QString(libvlc_errmsg());
#endif

    QMetaObject::invokeMethod(static_cast<QTVLCX::PlayerThread *>(object),
                              "vlcEvent",
                              Qt::QueuedConnection,
                              Q_ARG(const libvlc_event_t*, tmp));
}

void QTVLCX::PlayerThread::vlcEvent(const libvlc_event_t * event)
{
    if (m_mediaPlayer != 0)
    {
        switch (event->type)
        {
            case libvlc_MediaPlayerEndReached:
            {
                libvlc_state_t state = libvlc_media_list_player_get_state(m_mediaListPlayer);

                if (state != libvlc_Ended && state != libvlc_Stopped)
                {
                    qWarning() << "QTVLCX player stopped but in inconsistent state - " << state;
                    this->appendCommand(PlayerCommand::HardAbort);
                }
                else
                {
                    this->appendCommand(PlayerCommand::CheckEnd, state == libvlc_Ended);
                }
                break;
            }

            case libvlc_MediaPlayerStopped:
            {
                //qDebug() << QTVLC_TID << "QTVLCX: player has stopped: " << this->position() << " / " <<  this->duration();
                this->updateInternalState();
                emit stoppedPlayback();
                break;
            }

            case libvlc_MediaPlayerPlaying:
            {
                //qDebug() << QTVLC_TID << "QTVLCX: player has started";
                this->updateInternalState();
                emit startedPlayback();
                this->setPosition(m_pausePosition);
                break;
            }

            case libvlc_MediaPlayerPaused:
            {
                //qDebug() << QTVLC_TID << "QTVLCX: player has paused";
                emit pausedPlayback();
                this->appendCommand(PlayerCommand::PauseStop);
                break;
            }

            case libvlc_MediaPlayerEncounteredError:
            {
                //qDebug() << QTVLC_TID << "QTVLCX: libvlc_MediaPlayerEncounteredError: " << QString(libvlc_errmsg());
                break;
            }

            case libvlc_MediaPlayerLengthChanged:
            {
                emit this->durationChanged(event->u.media_duration_changed.new_duration);
            }

            default:
            {
                //qDebug() << "Event: " << libvlc_event_type_name(event->type);
                break;
            }
        }
    }

    delete event;
}

bool QTVLCX::PlayerThread::isAborting() const
{
    return m_state & PlayerState::Aborting || (!m_q.isEmpty() && m_q.first().command & PlayerCommand::Abort);
}

bool QTVLCX::PlayerThread::isWaiting() const
{
    return m_state & PlayerState::Waiting;
}

void QTVLCX::PlayerThread::updateInternalState()
{
    if (m_state == PlayerState::BootStrapping)
    {
        return;
    }
    else if (m_mediaListPlayer == 0)
    {
        m_state = PlayerState::Aborted;
    }
    else if (m_state != PlayerState::Aborted && m_state == PlayerState::Aborting)
    {
        // special use case for when we are trying to abort
        libvlc_state_t state = libvlc_media_list_player_get_state(m_mediaListPlayer);

        bool exitState = !(state == libvlc_Playing ||
                           state == libvlc_Buffering ||
                           state == libvlc_Opening);

        m_state = exitState ? PlayerState::Aborted : PlayerState::Aborting;
    }
    else
    {
        libvlc_state_t state = libvlc_media_list_player_get_state(m_mediaListPlayer);

        if (m_state & PlayerState::Waiting)
        {
            if (m_state == WaitingStart && state == libvlc_Playing)
            {
                m_state = PlayerState::Playing;
            }
            else if (m_state == WaitingStop && state == libvlc_Stopped)
            {
                m_state = PlayerState::Stopped;
            }
            else if (m_state == WaitingPause && (state == libvlc_Stopped ||
                                                 state == libvlc_Paused))
            {
                m_state = PlayerState::Stopped;
            }
            else if (m_state == WaitingFormatError && (state == libvlc_Ended ||
                                                       state == libvlc_Error))
            {
                emit this->error(tr("Failed to initialize playback"));
                this->appendCommand(PlayerCommand::HardAbort);
            }
        }
        else
        {
            m_state = (state == libvlc_Playing) ? PlayerState::Playing : PlayerState::Stopped;
        }
    }
}

void QTVLCX::PlayerThread::checkAbort()
{
    int now = QTime::currentTime().msecsSinceStartOfDay();

    if (m_waitTS == 0)
    {
        m_waitTS = now;
    }
    else if (now - m_waitTS > 1000)
    {
        // OK, we tried, this isnt working, just abort
        //qDebug() << "QTVLCX: check abort timeout";
        m_state = PlayerState::Aborted;
    }
}

void QTVLCX::PlayerThread::checkWait()
{
    static int errorCheckCount = 1;

    if (m_waitTS == 0)
    {
        m_waitTS = QTime::currentTime().msecsSinceStartOfDay();
        errorCheckCount = 0;
    }
    else
    {
        bool playing = this->isPlaying();

        libvlc_state_t state = libvlc_media_list_player_get_state(m_mediaListPlayer);

        if (playing && (m_state == WaitingStart))
        {
            m_state = PlayerState::Playing;
            errorCheckCount = 0;
        }
        else if (!playing && ((m_state == WaitingStop) || (m_state == WaitingPause)))
        {
            m_state = PlayerState::Stopped;
            errorCheckCount = 0;
        }
        else if (m_state == WaitingFormatError && (state == libvlc_Error ||
                                                   state == libvlc_Ended))
        {
            m_state = PlayerState::Stopped;
            errorCheckCount = 0;
        }
        else
        {
            // capped in case something goes wronger than wrong
            if ((QTime::currentTime().msecsSinceStartOfDay() - m_waitTS) > 2000)
            {
                errorCheckCount++;

                qWarning() << "QTVLCX waiting timeout - " << m_errString << " : count: " << errorCheckCount;

                int abort = 0; // 0 is no, 1 is hard, 2 is soft

                m_commandMutex.lock();

                // check for abort commands in queue, if found, go straight to abort
                for (int i = 0; i < m_q.size() && !abort; ++i)
                {
                    if (m_q.at(i).command & PlayerCommand::Abort)
                    {
                        abort = m_q.at(i).command == PlayerCommand::HardAbort ? 1 : 2;
                    }
                }

                if (abort > 0)
                {
                    m_q.clear();
                    m_q.prepend(PlayerEvent(abort == 1 ? PlayerCommand::HardAbort : PlayerCommand::SoftAbort));
                    // updateInternalState will make sure we are going to process this next
                }
                else
                {
                    m_q.prepend(m_waitEvent);

#ifdef Q_OS_ANDROID
                    // android can fail to start the player for some reason
                    // if this happens and we are in a waitstart situation,
                    // we can just kill the mediaplayer and start again
                    if (m_state == PlayerState::WaitingStart && (errorCheckCount == 5 || state == libvlc_Ended))
                    {
                        qWarning() << "QTVLCX: setting up to rerun bootstrap";
                        m_state = PlayerState::BootStrapping;
                    }
                    else
#endif
                    if (errorCheckCount >= 5) // 10 seconds if the above number hasnt changed
                    {
                        emit error("Failed to start player");
                        m_q.clear();
                        m_q.prepend(PlayerEvent(PlayerCommand::HardAbort));
                        m_state = PlayerState::Stopped;
                    }
                    else
                    {
                        m_q.prepend(m_waitEvent);
                    }
                }

                m_commandMutex.unlock();

                qWarning() << "QTVLCX waiting timeout - rerunning command : " << m_waitEvent.command << " : " << m_state << " : " << state;
                m_waitTS = QTime::currentTime().msecsSinceStartOfDay();

#ifdef Q_OS_ANDROID

                if (m_state & PlayerState::WaitingStart && state == libvlc_Ended)
                {
                    // can happen, hopefully setting the time back to 0 will help
                    // better that than nothing happening at all
                    qWarning() << "QTVLCX: resetting playback position to 0 to avoid looping error";
                    m_pausePosition = 0;
                    libvlc_media_player_set_time(m_mediaPlayer, 0);
                }
#endif

                this->updateInternalState();

                if (m_state & PlayerState::Waiting)
                {
                    // no good being in a waiting state anymore is it
                    m_state = (state == libvlc_Playing) ? PlayerState::Playing : PlayerState::Stopped;
                }
            }
        }
    }
}

void QTVLCX::PlayerThread::formatUpdated()
{
    if (m_state == PlayerState::WaitingFormatError)
    {
        libvlc_state_t state = libvlc_media_list_player_get_state(m_mediaListPlayer);
        m_state = (state == libvlc_Playing) ? PlayerState::Playing : PlayerState::Stopped;
    }
}

void QTVLCX::PlayerThread::bootstrap()
{
    qDebug() << "QTVLCX: bootstrap";

    this->ensureHasVLCInstance();
    this->ensureHasMediaPlayer();
    this->setSourceFromCached();

    m_state = PlayerState::Stopped;
}

////////////////////////////////////////////////////////////////////////////////

void QTVLCX::PlayerThread::run()
{
    // this start - stop - reset position is designed to trigger a format request callback

    if (m_state & PlayerState::BootStrapping)
    {
        m_commandMutex.lock();
        m_q.prepend(PlayerEvent(PlayerCommand::SetPosition, 0));
        m_q.prepend(PlayerEvent(PlayerCommand::Stop));
        m_q.prepend(PlayerEvent(PlayerCommand::StartFormat));
        m_commandMutex.unlock();
    }

    ////////////////////////////////////////////////////////////////////////////

    forever
    {
        if (m_state & PlayerState::BootStrapping)
        {
            this->bootstrap();
        }
        else if (m_state & PlayerState::Aborted)
        {
            if (m_closeOnAbort || m_parentDeadAbort)
            {
                this->closeVLC();
                qDebug() << "QTVLCX: hard abort";
            }
            else
            {
                qDebug() << "QTVLCX: soft abort";
            }

            m_condition.wakeAll();
            return;
        }
        else if (this->isAborting())
        {
            QThread::msleep(10);
            this->updateInternalState();
            this->checkAbort();
        }
        else if (this->isWaiting())
        {
            QThread::msleep(10);
            this->checkWait();
        }
        else
        {
            // not aborting, not waiting, try to consume a command
            QThread::msleep(10);

            if (m_q.isEmpty())
            {
                continue;
            }

            m_commandMutex.lock();
            PlayerEvent event = m_q.front();
            m_q.pop_front();
            m_commandMutex.unlock();

            // commands are not issues very frequently so it should be ok to update the
            // internal state on each call
            this->updateInternalState();

            if (!(m_state & (PlayerState::Aborted | PlayerState::Aborting | PlayerState::BootStrapping)))
            {
                qDebug() << "QTVLCX: command " << event.command << " : " << m_state << " : " << libvlc_media_list_player_get_state(m_mediaListPlayer);

                if (!this->processPlayerEvent(event))
                {
                    // only pop if the command was consumed, so add it back in basically
                    m_commandMutex.lock();
                    m_q.prepend(event);
                    m_commandMutex.unlock();
                }
        }
        }
    }
}

bool QTVLCX::PlayerThread::processPlayerEvent(const PlayerEvent & event)
{
    switch (event.command)
    {
        case PlayerCommand::Resume:
        case PlayerCommand::Start:
        case PlayerCommand::StartFormat:
        {
            if (m_state == PlayerState::Stopped)
            {
                if (event.command == PlayerCommand::StartFormat)
                {
                    m_state = PlayerState::WaitingFormatError;
                }
                else
                {
                    m_state = PlayerState::WaitingStart;
                }

                m_waitEvent = event;
                m_waitTS = 0;

                // always start from the beginning so we dont instantly stop because we might be at the end
                libvlc_media_player_set_time(m_mediaPlayer, 0);
                libvlc_media_list_player_play(m_mediaListPlayer);
                //libvlc_media_player_set_time(m_mediaPlayer, m_pausePosition);

                //qDebug() << "QTVLCX: set position to : " << m_pausePosition << " : " << ts();

                return true;
            }
            else
            {
                bool playing = this->isPlaying();

                if (playing)
                {
                    emit this->startedPlayback();
                }

                return playing;
            }
        }

        case PlayerCommand::Stop:
        {
            if (m_state == PlayerState::Playing)
            {
                m_state = PlayerState::WaitingStop;
                m_waitTS = 0;

                m_pausePosition = 0;

                m_stopping = true;
                libvlc_media_list_player_stop(m_mediaListPlayer);
                m_stopping = false;

                return true;
            }
            else
            {
                bool playing = this->isPlaying();

                if (!playing)
                {
                    emit this->stoppedPlayback();
                }

                return !playing;
            }
        }

        case PlayerCommand::PauseStop:
        {
            m_stopping = true;
            libvlc_media_list_player_stop(m_mediaListPlayer);
            m_stopping = false;

            return true;
        }

        case PlayerCommand::Pause:
        {
            if (m_state == PlayerState::Playing)
            {
                m_state = PlayerState::WaitingPause;
                m_waitTS = 0;

                //libvlc_media_player_set_pause(m_mediaPlayer, 1);
                libvlc_media_list_player_pause(m_mediaListPlayer);
                m_pausePosition = libvlc_media_player_get_time(m_mediaPlayer);

                m_checkPosition = true;
            }

            return true;
        }

        case PlayerCommand::Abort:
        case PlayerCommand::SoftAbort:
        case PlayerCommand::HardAbort:
        {
            m_closeOnAbort = event.command == PlayerCommand::HardAbort;

            // in case we are in soft abort, keep track of the current time
            m_pausePosition = libvlc_media_player_get_time(m_mediaPlayer);

            PlayerState state = m_state;
            m_state = PlayerState::Aborting;

            if (state == PlayerState::Stopped)
            {
                return true;
            }

            // playing, so we need to stop playback first
            m_stopping = true;
            libvlc_media_list_player_stop(m_mediaListPlayer);
            m_stopping = false;

            return true;
        }

        case PlayerCommand::SetPosition:
        {
            m_checkPosition = true;
            m_pausePosition = qMax(0, event.data.toInt());
            libvlc_media_player_set_time(m_mediaPlayer, static_cast<libvlc_time_t>(m_pausePosition));

            //qDebug() << "QTVLCX : set position to : " << m_pausePosition;

            // if we are not playing, as we do not do display updates yet, we should notify our position has changed manually here
            if (!this->isPlaying())
            {
                emit this->positionChanged(m_pausePosition);
            }

            return true;
        }

        case PlayerCommand::SetVolume:
        {
            libvlc_audio_set_volume(m_mediaPlayer, qMin<int>(qMax<int>((int)m_volume, 0), 100));
            return true;
        }

        case PlayerCommand::FakePosition:
        {
            emit this->positionChanged(event.data.toInt());
            return true;
        }

        case PlayerCommand::CheckEnd:
        {
            libvlc_state_t state = libvlc_media_list_player_get_state(m_mediaListPlayer);

            //qDebug() << "QTVLCX: CheckEnd: " << playing << " -> " << m_state << " : " << time << " / " << dur << " -> " << event.data.toBool() << " : " << state;

            if (event.data.toBool() && (state == libvlc_Ended ||
                                        state == libvlc_Stopped))
            {
                qDebug() << "QTVLCX: reached end of playback";

                this->stopPlayer();
                this->setPosition(0);

                if (m_loop)
                {
                    //qDebug() << "QTVLCX - looping";
                    this->startPlayer();
                }
                else
                {
                    this->appendCommand(PlayerCommand::FakePosition, this->duration());
                }
            }
            else
            {
                qWarning() << "QTVLCX: ignoring check end, called when player state is inconsistent: " << event.data.toBool() << " : " << state;
            }

            return true;
        }

        default:
        {
            return true;
        }
    }
}

////////////////////////////////////////////////////////////////////////////////

QTVLCX::PlayerThread::PlayerEvent::PlayerEvent()
    : command(PlayerCommand::NoCommand)
{
}

QTVLCX::PlayerThread::PlayerEvent::PlayerEvent(QTVLCX::PlayerThread::PlayerCommand command)
    : command(command)
{
}

QTVLCX::PlayerThread::PlayerEvent::PlayerEvent(QTVLCX::PlayerThread::PlayerCommand command, QVariant data)
    : command(command),
      data(data)
{
}
