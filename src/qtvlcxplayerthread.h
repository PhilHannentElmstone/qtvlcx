#ifndef QTVLCXPLAYERTHREAD_H
#define QTVLCXPLAYERTHREAD_H

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QVariant>
#include <QSize>
#include <QQueue>

#include <vlc/vlc.h>

namespace QTVLCX
{
    class PlayerThread : public QThread
    {
            Q_OBJECT

        public:

            enum PlayerCommand
            {
                NoCommand = 0,
                Start = 1,
                StartFormat = 2 | Start,
                Stop = 4,
                Pause = 8,
                Resume = 16,
                SetPosition = 32,
                PauseStop = 64,
                Abort = 128,
                HardAbort = 256 | Abort,
                SoftAbort = 512 | Abort,
                FakePosition = 1024,
                CheckEnd = 2048,
                SetVolume = 4096
            };
            Q_ENUM(PlayerCommand)

            enum PlayerState
            {
                Stopped = 1,
                Playing = 2,
                Waiting = 4,
                WaitingStart = 8 | Waiting, // waiting for start
                WaitingStop = 16 | Waiting,  // waiting for stop
                WaitingPause = 32 | WaitingStop,  // waiting for pause
                WaitingFormatError = 64 | Waiting,
                Aborting = 128,
                Aborted = 256,
                BootStrapping = 512
            };
            Q_ENUM(PlayerState)

            PlayerThread(QObject *parent = 0);
            ~PlayerThread();

            void                closeMedia();
            void                closeVLC();

            void                startPlayer();
            void                stopPlayer();
            void                pausePlayer();
            void                resumePlayer();

            void                setPosition(qint64 position);

            qint64              duration() const;
            qint64              position() const;

            int                 nativeWidth() const;
            int                 nativeHeight() const;

            QString             source() const;
            void                setSource(const QString & source);

            bool                isPlaying() const;

            void                setLooping(bool loop);

            // 0 == mute, 100 == 0dB
            int                 volume() const;
            void                setVolume(int volume);

            ////////////////////////////////////////////////////////////////////

            void                debug();

            void                softAbort(bool block = true);
            void                conditionalRestart();

        signals:

            void                imageReady(const QImage &image);

            void                positionChanged(qint64 position);
            void                durationChanged(qint64 duration);

            void                stoppedPlayback();
            void                startedPlayback();
            void                pausedPlayback();

            void                sizeChanged(QSize size);

            void                error(const QString & str = QString());

        public slots:

            void                imageConsumed();

        protected:

            void                run() Q_DECL_OVERRIDE;

        private slots:

            void                vlcEvent(const libvlc_event_t * event);

            void                setupFormat(char * chroma,
                                            unsigned int * width,
                                            unsigned int * height,
                                            unsigned int * pitches,
                                            unsigned int * lines);

        private:

            struct PlayerEvent
            {
                    PlayerEvent();
                    PlayerEvent(PlayerCommand command);
                    PlayerEvent(PlayerCommand command, QVariant data);

                    PlayerCommand       command;
                    QVariant            data;
            };

            bool                setSourceFromCached();

            void                ensureHasVLCInstance();
            void                ensureHasMediaPlayer();

            static unsigned int vlcCBFormat(void ** object,
                                            char * chroma,
                                            unsigned int * width,
                                            unsigned int * height,
                                            unsigned int * pitches,
                                            unsigned int * lines);

            static void    *    vlcCBLock(void * object, void ** planes);
            static void         vlcCBUnlock(void * object, void * picture, void * const * planes);
            static void         vlcCBDisplay(void * object, void * picture);
            static void         vlcCBEvent(const libvlc_event_t * event, void * object);
            static void         vlcLogEvent(void *data, int level, const libvlc_log_t *ctx, const char *fmt, va_list args);

            void                lock();
            void                unlock();

            void                flipBacking();

            bool                isAborting() const;
            bool                isWaiting() const;

            void                checkWait();
            void                checkAbort();
            void                formatUpdated();

            void                updateInternalState();

            /// returns true if the event was consumed
            bool                processPlayerEvent(const PlayerEvent & event);

            void                appendCommand(PlayerCommand cmd, QVariant data = QVariant());

            void                bootstrap();

            void                blockWaitAbort();

            void                eventCompress();

            void                configure(const QString & source);

            ////////////////////////////////////////////////////////////////////

            QWaitCondition      m_condition;

            QQueue<PlayerEvent> m_q;

            volatile PlayerState m_state;

            QString             m_source;

            quint32             m_width;
            quint32             m_height;

            volatile qint64     m_pausePosition;
            volatile bool       m_checkPosition;
            volatile bool       m_stopping;

            volatile bool       m_loop;
            volatile int        m_volume;

            PlayerEvent         m_waitEvent;
            int                 m_waitTS;

            bool                m_closeOnAbort;
            volatile bool       m_parentDeadAbort;
            bool                m_configured;

            libvlc_media_player_t *         m_mediaPlayer;
            libvlc_media_list_player_t *    m_mediaListPlayer;
            libvlc_media_list_t *           m_mediaList;
            libvlc_media_t   *              m_media;
            libvlc_instance_t *             m_vlcInstance;

            QString             m_errString;

            mutable QMutex      m_commandMutex;

#ifdef Q_OS_ANDROID
            QImage       *      m_backingImgA;
            QImage       *      m_backingImgB;
            QImage *            m_fillPtr;
#else
            char         *      m_pixelBuffA[3];
            char         *      m_pixelBuffB[3];
            char         *      m_pixelPtr;
            char         *      m_pixelPtrAlt;
#endif
    };
}

#endif // QTVLCXPLAYERTHREAD_H
