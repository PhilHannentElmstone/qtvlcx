#ifndef QTVLCXDISCOVERYAGENT_P_H
#define QTVLCXDISCOVERYAGENT_P_H

#if defined(_MSC_VER)
#include <BaseTsd.h>
typedef SSIZE_T ssize_t;
#endif

#include "qtvlcxglobal.h"

#include <QObject>

struct libvlc_instance_t;

namespace QTVLCX
{
    class DiscoveryAgent;

    class DiscoveryAgentPrivate : public QObject
    {
            Q_OBJECT

        public:

            DiscoveryAgentPrivate();

            libvlc_instance_t * m_vlcInstance;

        private:

            DiscoveryAgent    *   q_ptr;
            Q_DECLARE_PUBLIC(DiscoveryAgent)
    };
}

#endif // QTVLCXDISCOVERYAGENT_P_H
