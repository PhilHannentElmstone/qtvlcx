#include "qtvlcxconversionthread.h"

#include <QDebug>

QTVLCX::ConversionThread::ConversionThread(QObject *parent)
    : QThread(parent),
      m_abort(false)
{
}

QTVLCX::ConversionThread::~ConversionThread()
{
    m_mutex.lock();
    m_abort = true;
    m_mutex.unlock();

    this->wait();
}

void QTVLCX::ConversionThread::convert(const QImage & img)
{
    QMutexLocker locker(&m_mutex);

    if (m_abort)
    {
        qWarning() << "Called QTVLCX::ConversionThread::convert after abort";
        return;
    }

    this->m_img = QImage(img.constBits(), img.width(), img.height(), img.format());
    this->m_process = true;

    if (!this->isRunning())
    {
        this->start();
    }
}

void QTVLCX::ConversionThread::abort()
{
    m_abort = true;
}

void QTVLCX::ConversionThread::reset()
{
    m_abort = false;
}

void QTVLCX::ConversionThread::run()
{
    forever
    {
        m_mutex.lock();

        if (m_abort)
        {
            m_mutex.unlock();
            return;
        }

        if (m_process)
        {
            QImage img = QImage(m_img.constBits(), m_img.width(), m_img.height(), m_img.format());
            m_process = false;

            m_mutex.unlock();

            QPixmap pix = QPixmap::fromImage(img.rgbSwapped());
            emit this->consume();
            emit this->converted(pix);
        }
        else
        {
            m_mutex.unlock();
        }
    }
}
