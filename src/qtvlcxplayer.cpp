
#include "qtvlcxplayer.h"

#include <vlc/vlc.h>

#include "qtvlcxplayerthread.h"
#include "qtvlcxconversionthread.h"

#include <QImage>
#include <QFileInfo>

#include <QDebug>
#include <QPainter>

QTVLCX::Player::Player(QObject * parent)
    : QObject(parent),
      m_playerThread(*(new PlayerThread(this))),
      m_convertThread(*(new ConversionThread(this)))
{
    m_pixmap = QPixmap(1, 1); // need a size to be able to do the fill
    m_pixmap.fill(Qt::black);

#ifdef QT_DEBUG
    this->paintDebugNotifier();
#endif

    connect(&m_playerThread, &PlayerThread::imageReady, &m_convertThread, &ConversionThread::convert, Qt::DirectConnection);
    connect(&m_convertThread, &ConversionThread::converted, this, &Player::updatePixmap, Qt::QueuedConnection);

    connect(&m_convertThread, &ConversionThread::consume, &m_playerThread, &PlayerThread::imageConsumed, Qt::DirectConnection);

    connect(&m_playerThread, &PlayerThread::positionChanged, this, &Player::positionChanged, Qt::QueuedConnection);
    connect(&m_playerThread, &PlayerThread::durationChanged, this, &Player::durationChanged, Qt::QueuedConnection);

    connect(&m_playerThread, &PlayerThread::sizeChanged, this, &Player::sizeChanged, Qt::QueuedConnection);

    connect(&m_playerThread, &PlayerThread::stoppedPlayback, this, &Player::stopped, Qt::QueuedConnection);
    connect(&m_playerThread, &PlayerThread::startedPlayback, this, &Player::playing, Qt::QueuedConnection);
    connect(&m_playerThread, &PlayerThread::pausedPlayback, this, &Player::paused, Qt::QueuedConnection);

    connect(&m_playerThread, &PlayerThread::error, this, &Player::error, Qt::QueuedConnection);
}

QString QTVLCX::Player::processInputString(const QString & str)
{
    QString s = str;

    // we also check for opencv based camera urls, which are just numbers
    bool isNumber = true;

    int index = 0;
    while (isNumber && index < str.length())
    {
        if (!str.at(index).isDigit())
        {
            isNumber = false;
        }

        index++;
    }

#ifdef Q_OS_WIN

    // if this is a local file
    // convert all forward spaces to backspaces

    QFileInfo info (str);
    if (info.exists() && info.isFile())
    {
        qDebug() << "QTVLCX slash replacing input source mrl";
        s.replace("/", "\\");
    }

    // check for wrong operating system, convert to the system default

    if (str.startsWith("v4l2") || isNumber)
    {
        // convert to the dshow default
        s = "dshow:// :dshow-vdev= :dshow-adev= :live-caching=300";
    }

#else

    if (str.startsWith("dshow") || isNumber)
    {
        // convert to the v4l2 default
        s = "v4l2://";
    }

#endif


    qDebug() << "QTVLCX::Player::processInputString - " << str << " -> " << s;

    return s;
}

void QTVLCX::Player::closeMedia()
{
    m_playerThread.closeMedia();
}

void QTVLCX::Player::loadMedia()
{
    m_convertThread.reset();
    m_playerThread.conditionalRestart();
    // m_convertThread starts automatically when required
}

void QTVLCX::Player::unloadMedia()
{
    m_playerThread.softAbort();
    m_convertThread.abort();
}

qint64 QTVLCX::Player::duration() const
{
    return m_playerThread.duration();
}

qint64 QTVLCX::Player::position() const
{
    return m_playerThread.position();
}

void QTVLCX::Player::setPosition(qint64 position)
{
    m_playerThread.setPosition(position);
}

void QTVLCX::Player::setLooping(bool loop)
{
    m_playerThread.setLooping(loop);
}

int QTVLCX::Player::volume() const
{
    return m_playerThread.volume();
}

void QTVLCX::Player::setVolume(int volume)
{
    m_playerThread.setVolume(volume);
}

void QTVLCX::Player::debug()
{
    m_playerThread.debug();
}

// TODO: this can cause multiple updates in a single event loop round,
// which we obviously dont NEED to do, just not sure of best way to do otherwise

// need something like set flag to get next image when requested, do logic in getFrame()

void QTVLCX::Player::updatePixmap(const QPixmap pix)
{
    //qDebug() << QTVLC_TID << " QTVLCX - pixmap updated";
    m_pixmap = pix;

#ifdef QT_DEBUG
    this->paintDebugNotifier();
#endif

    emit frameChanged();
}

void QTVLCX::Player::paintDebugNotifier()
{
    /*
    QPainter p(&m_pixmap);
    p.setPen(Qt::white);
    QFont f = p.font();
    f.setPointSizeF(16);
    p.setFont(f);

    static int i = -1;
    i++;

    p.drawText(m_pixmap.rect(), Qt::AlignCenter, QString("Image #") + QString::number(i));
    */
}

int QTVLCX::Player::width() const
{
    return m_playerThread.nativeWidth();
}

int QTVLCX::Player::height() const
{
    return m_playerThread.nativeHeight();
}

QPixmap QTVLCX::Player::currentFrame()
{
    //qDebug() << QTVLC_TID << " QTVLCX - currentFrame()";
    return m_pixmap;
}

QString QTVLCX::Player::source() const
{
    return m_source;
}

void QTVLCX::Player::setSource(const QString & source)
{
    QString newSource = QTVLCX::Player::processInputString(source);

    if (m_source != newSource)
    {
        // reset the blackout pixmap
        m_pixmap = QPixmap(1, 1); // need a size to be able to do the fill
        m_pixmap.fill(Qt::black);
    }

    m_source = newSource;
    m_playerThread.setSource(m_source);
}

bool QTVLCX::Player::isPlaying() const
{
    return m_playerThread.isPlaying();
}

void QTVLCX::Player::start()
{
    m_playerThread.startPlayer();
}

void QTVLCX::Player::stop()
{
    m_playerThread.stopPlayer();
}

void QTVLCX::Player::pause()
{
    m_playerThread.pausePlayer();
}

void QTVLCX::Player::resume()
{
    m_playerThread.resumePlayer();
}

