
#include "qtvlcxitem.h"
#include "qtvlcxitem_p.h"

#include <vlc/vlc.h>

#include "qtvlcxplayer.h"

#include <QPainter>

#include <QDebug>
#include <QTime>

QTVLCX::ItemPrivate::ItemPrivate()
    : m_paintMode(PaintModeQPainter),
      m_attached(true)
{
    m_player = new QTVLCX::Player();
}

QTVLCX::ItemPrivate::~ItemPrivate()
{
    delete m_player;
}

void QTVLCX::ItemPrivate::closeMedia()
{
    m_player->closeMedia();
}

void QTVLCX::ItemPrivate::attachPlayer()
{
    connect(m_player, &QTVLCX::Player::stopped, q_ptr, &QTVLCX::Item::stopped);
    connect(m_player, &QTVLCX::Player::playing, q_ptr, &QTVLCX::Item::playing);
    connect(m_player, &QTVLCX::Player::paused, q_ptr, &QTVLCX::Item::paused);
    connect(m_player, &QTVLCX::Player::error, q_ptr, &QTVLCX::Item::error);

    connect(m_player, &QTVLCX::Player::sizeChanged, q_ptr, [this](QSize size)
    {
        q_ptr->prepareGeometryChange();
        emit q_ptr->sizeChanged(size);
    });

    connect(m_player, &QTVLCX::Player::frameChanged, q_ptr, &Item::updateView);

    connect(m_player, &QTVLCX::Player::positionChanged, q_ptr, &Item::positionChanged);
    connect(m_player, &QTVLCX::Player::durationChanged, q_ptr, &Item::durationChanged);
}

////////////////////////////////////////////////////////////////////////////////

QTVLCX::Item::Item(QGraphicsItem * parent)
    : QGraphicsObject(parent),
      d_ptr(new QTVLCX::ItemPrivate)
{
    d_ptr->q_ptr = this;
    d_ptr->attachPlayer();
}

QTVLCX::Item::~Item()
{
    delete d_ptr;
}

QString QTVLCX::Item::source() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player->source();
}

void QTVLCX::Item::setSource(const QString & source)
{
    Q_D(QTVLCX::Item);
    d->m_player->setSource(source);
}

bool QTVLCX::Item::isPlaying() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player->isPlaying();
}

QString QTVLCX::Item::getError() const
{
    return QString(libvlc_errmsg());
}

void QTVLCX::Item::start()
{
    Q_D(QTVLCX::Item);
    d->m_player->start();
}

void QTVLCX::Item::stop()
{
    Q_D(QTVLCX::Item);
    d->m_player->stop();
}

void QTVLCX::Item::pause()
{
    Q_D(QTVLCX::Item);
    d->m_player->pause();
}

void QTVLCX::Item::resume()
{
    Q_D(QTVLCX::Item);
    d->m_player->resume();
}

QRectF QTVLCX::Item::boundingRect() const
{
    //Q_D(const QTVLCX::Item);
    //return QRectF(0, 0, d->m_player->width(), d->m_player->height());
    QRectF pr = this->parentItem()->boundingRect();
    return QRectF(0, 0, pr.width(), pr.height());
}

QPainterPath QTVLCX::Item::shape() const
{
    QPainterPath p;
    p.addRect(this->boundingRect());
    return p;
}

int QTVLCX::Item::nativeWidth() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player->width();
}

int QTVLCX::Item::nativeHeight() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player->height();
}

QSize QTVLCX::Item::nativeResolution() const
{
    Q_D(const QTVLCX::Item);
    return QSize(d->m_player->width(), d->m_player->height());
}

qint64 QTVLCX::Item::duration() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player->duration();
}

qint64 QTVLCX::Item::position() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player->position();
}

void QTVLCX::Item::setPosition(qint64 position)
{
    Q_D(QTVLCX::Item);
    d->m_player->setPosition(position);
}

void QTVLCX::Item::setLooping(bool loop)
{
    Q_D(QTVLCX::Item);
    d->m_player->setLooping(loop);
}

int QTVLCX::Item::volume() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player->volume();
}

QTVLCX::Player * QTVLCX::Item::player() const
{
    Q_D(const QTVLCX::Item);
    return d->m_player;
}

void QTVLCX::Item::redirectPlayerVideo(bool redirect)
{
    Q_D(QTVLCX::Item);

    if (redirect)
    {
        d->m_attached = false;
    }
    else
    {
        d->m_attached = true;
    }
}

void QTVLCX::Item::setVolume(int volume)
{
    Q_D(QTVLCX::Item);
    d->m_player->setVolume(volume);
}

void QTVLCX::Item::updateView()
{
    Q_D(const QTVLCX::Item);

    if (d->m_attached)
    {
        this->update();
    }
    else
    {
        emit this->frameChanged();
    }
}

void QTVLCX::Item::loadMedia()
{
    Q_D(QTVLCX::Item);
    d->m_player->loadMedia();
}

void QTVLCX::Item::unloadMedia()
{
    Q_D(QTVLCX::Item);
    d->m_player->unloadMedia();
}

void QTVLCX::Item::paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget)
{
    Q_D(QTVLCX::Item);

    QRectF boundingRect = this->boundingRect();

    //painter->setBrush(Qt::green);
    //painter->drawRect(boundingRect);

    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->drawPixmap(boundingRect.toRect(), d->m_player->currentFrame());
}

void QTVLCX::Item::debug()
{
    Q_D(QTVLCX::Item);
    d->m_player->debug();
}

